# Containerization Hands-On

## Getting Started

Go to the Console and launch a virtual VM with the instance template 'cnad-container-module'
Use a unique name for example Firtsname-Lastname-VM


### Prerequisites

If you haven't done so please configure Git for the first time as follows

```
git config --global user.name "Lastname, Firtsname"
git config --global user.email "firstname.lastname@accenture.com"
```

Clone this respository

```
git clone https://firstname.lastname@innersource.accenture.com/scm/asgrcp/containerization-hands-on.git
```


### Connecting to VM instance

**Windows**
- Use Putty to connect with the VM you just created which already containes a public key for this course
- To connect with the instance use the Private Key provided in the folder SSH Access
- The hostname needs to look like this cnad-attendee@externalIP
- You find the external IP of your instance in the console

**Mac**
- To connect with the instance use the Private Key provided in the folder SSH Access
- Go to the terminal and use the ssh command as follows
 ```
 ssh -i [PATH_TO_PRIVATE_KEY] cnad-attendee@[EXTERNAL_IP_ADDRESS]
 ```
- You find the external IP of your instance in the console

* [Detailed instruction](https://cloud.google.com/compute/docs/instances/connecting-advanced#thirdpartytools)